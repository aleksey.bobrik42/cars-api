image: openjdk:12-alpine

variables:
  ARTIFACT_NAME: cars-api-v$CI_PIPELINE_IID.jar
  APPLICATION_NAME: cars-api

stages:
  - build
  - test
  - deploy

build-job:
  stage: build
  before_script:
    - echo "Building..."
  script:
    - sed -i "s/CI_PIPELINE_IID/$CI_PIPELINE_IID/" ./src/main/resources/application.yml
    - sed -i "s/CI_COMMIT_SHORT_SHA/$CI_COMMIT_SHORT_SHA/" ./src/main/resources/application.yml
    - sed -i "s/CI_COMMIT_BRANCH/$CI_COMMIT_BRANCH/" ./src/main/resources/application.yml
    - ./gradlew build -x test
    - mv ./build/libs/cars-api.jar ./build/libs/$ARTIFACT_NAME
  artifacts:
    paths:
      - ./build/libs

code-quality-job:
  stage: test
  before_script:
    - echo "Code quality test..."
  script:
    - ./gradlew pmdMain pmdTest
  artifacts:
    when: always
    paths:
      - build/reports/pmd

unit-tests-job:
  stage: test
  before_script:
    - echo "Unit tests..."
  script:
    - ./gradlew test
  artifacts:
    when: always
    paths:
      - build/reports/tests
    reports:
      junit: build/reports/tests/test/*.xml

smoke-test-job:
  stage: test
  before_script:
    - echo "Smoke test..."
    - apk --no-cache add curl
  script:
    - java -jar ./build/libs/$ARTIFACT_NAME &
    - sleep 20
    - curl http://localhost:5000/actuator/health | grep "UP"

deploy-job:
  stage: deploy
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  before_script:
    - echo "Deploying..."
    - yum install -y jq
    - aws configure set region eu-north-1
  script:
    - aws s3 cp ./build/libs/$ARTIFACT_NAME s3://$S3_BUCKET/$ARTIFACT_NAME
    - aws elasticbeanstalk create-application-version --application-name $APPLICATION_NAME --version-label $CI_PIPELINE_IID --source-bundle S3Bucket=$S3_BUCKET,S3Key=$ARTIFACT_NAME
    - CNAME=$(aws elasticbeanstalk update-environment --application-name $APPLICATION_NAME --environment-name "prod" --version-label=$CI_PIPELINE_IID | jq '.CNAME' --raw-output)
    - sleep 60
    - echo $CNAME
    - curl http://$CNAME/actuator/health | grep "UP"
    - curl http://$CNAME/actuator/info | grep $CI_PIPELINE_IID
